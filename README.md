# Service Usage

* HZDR Collabtex

## Plotting

* Plotting is be performed in the [Plotting project](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci).

## Data

| Data                                         | Unit            | Weighting            | Comment              |
|----------------------------------------------|-----------------|----------------------|----------------------|
| active_users_csv.number_of_users             | Quantity/Number | not yet considered   |                      |
| active_users_csv.last_active_1_days          | Quantity/Number | not yet considered   |                      |
| active_users_csv.last_active_7_days          | Quantity/Number | not yet considered   |                      |
| active_users_csv.last_active_14_days         | Quantity/Number | not yet considered   |                      |
| active_users_csv.last_active_30_days         | Quantity/Number | not yet considered   |                      |
| active_users_csv.last_active_60_days         | Quantity/Number | not yet considered   |                      |
| overleaf_email_domains_csv                   | Quantity/Number | not yet considered   | Origin by domain     |
| overleaf_user_origins_csv                    | Quantity/Number | not yet considered   | Origin by VO         |
| project_statistics_csv.number_of_projects    | Quantity/Number | not yet considered   |                      |
| project_statistics_csv.last_active_1_days    | Quantity/Number | not yet considered   |                      |
| project_statistics_csv.last_active_7_days    | Quantity/Number | not yet considered   |                      |
| project_statistics_csv.last_active_14_days   | Quantity/Number | not yet considered   |                      |
| project_statistics_csv.last_active_30_days   | Quantity/Number | not yet considered   |                      |
| project_statistics_csv.last_active_60_days   | Quantity/Number | not yet considered   |                      |


## Schedule

* daily


